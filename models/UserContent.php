<?php

namespace app\models;
use \yii;
use \yii\helpers\FileHelper;
use \yii\helpers\Url;


class UserContent extends AwarenessFileContent{
	
	public static $SCENARIO_CREATE_DOSSIER = 'Content for dossier';

	/**
     * @inheritdoc
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = [['title'], 'required'];
		$rules[] = [['title'], 'string', 'skipOnEmpty'=>false, 'min'=>8, 'max'=>40];
		$rules[] = [['description'], 'string', 'min'=>10, 'max'=>1000];
		$rules[] = [['description'], function ($attribute, $params, $validator) {
				//description is required if no file is submitted
                if (empty($this->$attribute) && empty($this->file)) {
                    $this->addError($attribute, 'Description cannot be empty in a text only post');
                }

            },
			'skipOnEmpty' => false];
        return $rules;
    }
	
	public function mimeTypes()
    {
		$mimes = $this->contentMimeTypes();

        return array_merge($mimes['audio'], $mimes['video'], $mimes['image']);
    }

	public static function contentMimeTypes(){
		return[
			'audio' => ['audio/wav','audio/wave', 'audio/x-wav','audio/x-wave','audio/mpeg', 'audio/mpeg3', 'audio/x-mpeg-3','video/mpeg','video/x-mpeg'],
			'video' => ['video/mp4', 'video/x-m4v'],
			'image' => ['image/jpeg', 'image/jpg']
		];
	}

	
	public static function getType(){
		return null;
	}
	
	public $contentData;
	
	public function resources()
	{
		$resources = ['source'];
		if($this->type == 'audio'){
			$resources[] = 'waveform';
		}
		if($this->type == 'video'){
			$resources[] = 'poster';
		}
		return $resources;
	}
	
	public static function tableName()
	{
		return 'user_content';
	}
	
	public function __construct()
	{
		parent::__construct();
		$this->contentData = null;
	}
	
	public function save($runValidation = true, $attributesNames = null)
	{
		if(parent::save($runValidation, $attributesNames))
		{
			if($this->contentData)
			{
				$this->contentData->id = $this->id;
				return $this->contentData->save(false, $attributesNames);
			}
			return true;
		}
		return false;
	}
	
	public function validate($attributeNames = null, $clearErrors = true)
	{
		$res = true;
		if($this->contentData){
			$res = $this->contentData->validate($attributeNames, $clearErrors);
		}
		
		return $res && parent::validate($attributeNames, $clearErrors);
	}
	
	public static function find()
	{

		$query = parent::find();
		if(static::getType()){
			$query = $query->andWhere(['type'=>static::getType()]);
		}
			
		return $query;
	}


	// public function getAttributes($names = null, $except = []){
	// 	$attributes = parent::getAttributes($names, $except);


	// 	if($this->contentData && (!$names || (array_key_exists('content_data',$names) && !array_key_exists('content_data', $except))))
	// 	{
	// 		$attributes = array_merge($attributes, $this->contentData->getAttributes());
	// 	}

	// 	return $attributes;

	// }

	public static function getFileType($filePath){
		$fileMimeType = self::getMimeType($filePath);
		foreach(self::contentMimeTypes() as $type => $mime){

			$isThisTheType = (is_string($mime) && $mime == $fileMimeType) ||
								(is_array($mime) && in_array($fileMimeType, $mime));
			if($isThisTheType){
				return $type;
			}
		}

		return null;
	}

	public function getResourceUri($resourceName){
		$resourceUris = [
			'source' => $this->file_path
		];

		if($this->type == 'audio'){
			$dir = dirname($this->file_path);
			$audioFileName = basename($this->file_path);
			$audioFileName = substr($audioFileName, 0, strrpos($audioFileName, "."));
			$resourceUris ['waveform'] = "$dir/$audioFileName.waveform.json"; 
		}elseif($this->type == 'video'){
			$dir = dirname($this->file_path);
			$audioFileName = basename($this->file_path);
			$audioFileName = substr($audioFileName, 0, strrpos($audioFileName, "."));
			$resourceUris['poster'] = "$dir/$audioFileName.poster.jpeg"; 
		}
		

		if(isset($resourceUris[$resourceName])){
			return $resourceUris[$resourceName];
		}

		return null;
	}


	 public function afterSave( $insert, $changedAttributes)
    {
        parent::afterSave( $insert, $changedAttributes );
        if($insert){
			 \app\commands\PostProcessingController::launch($this->id);
		}
    }

	public function beforeDelete()
	{
    	if (parent::beforeDelete()) {
        	
			$appDir = Yii::getAlias('@app');

			switch($this->type){
				case 'audio':
					//remove waveform file
					$file = $appDir.$this->getResourceUri('waveform');
					if(file_exists($file) && unlink($file)){
						echo "Deleted: $file";
					}
				
				case 'video':
				case 'image':
					//remove content file
					$file = $appDir.$this->getResourceUri('source');
					if(file_exists($file) && unlink($file)){
						echo "Deleted: $file";
					}
				default:
					break;
			}
        	return true;
    	} else {
        	return false;
    	}
	}


	public function fields(){
		$fields = [
			'id',
			'creator' => function($user){
				return $user->getCreator();
			}, 
			'type',
			'title',
			'description',
			'created_at',
			'updated_at',
			'awareness_context',
			'delete' => function($model, $field){ return Url::to(['user-content/delete', 'id'=> $model->id]); },
		];

		foreach($this->resources() as $resource)
		{
			$fields[$resource] = function($model, $resource) { 
			return Url::to([
				'user-content/resource',
				'type'=> $model->type,
				'id'=> $model->id,
					'resource'=>$resource
				]);
			};
		}

		if($this->awareness_context){

		}
		

		return $fields;
	}

	public static function getMimeType($filePath){
		return FileHelper::getMimeType($filePath, null, false);
	}

	public static function getExtensionFor($filePath){
		$fileMimeType = self::getMimeType($filePath);
		$exts = FileHelper::getExtensionsByMimeType($fileMimeType);
		return $exts[0];
	}

	public static function generateRandomFilename($ext){
		return uniqid('').".$ext";
	}

	public function getCreator(){
        return User::findOne($this->creator);
    }
}