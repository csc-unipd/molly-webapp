<?php

namespace app\models;

use Yii;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{


    public static $SCENARIO_PUBLIC = 'public';
    public $authKey;
    // public $accessToken;
    public $password;
    public $passwordConfirmation;

    // public function scenarios(){
    //     return [
    //         self::$SCENARIO_PUBLIC => [
    //             'id',
    //             'first_name',
    //             'last_name'
    //         ]
    //     ];
    // }

    public function rules(){
        return [
           [['username'], 'required'],
           [['first_name','last_name'], 'required'],
           [['first_name','last_name'], 'match', 'pattern'=>'/^[a-z ,.\'-]+$/i'],
           [['username'], 'match', 'pattern'=>'/^[a-z][a-z0-9]{3,20}$/i'],
           [['password'], 'required'],
           [['role'], 'required'],
           [['password'], 'string', 'min'=> 8, 'max'=>20],          
           [['email'], 'email'],
           [['email', 'username'], 'unique'],
        [['passwordConfirmation'], 'safe'],
        ];
    }

    public static function tableName(){
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        $user = self::findOne($id);
        return $user?$user: null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        // foreach (self::$users as $user) {
        //     if ($user['accessToken'] === $token) {
        //         return new static($user);
        //     }
        // }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    public static function findByEmail($email)
    {
        $user = static::findOne(['email' => $email]);

        return $user ? $user: null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return false;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->getSecurity()->validatePassword($password, $this->password_hash);
    }


    public function fields(){
        return [
            'id',
            'first_name',
            'last_name',
            'full_name' => function($user){
                return $user->getFullName();
            }
        ];
    }

    public function getFullName(){

        return ucwords( "$this->first_name $this->last_name");
    }
}
