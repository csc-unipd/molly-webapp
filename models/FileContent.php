<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;

class FileContent extends ActiveRecord 
{

    static $MAX_FILE_SIZE_MB = 100;
    static $FILE_CONTENTS_DIR = 'users-data';
    
    public $parentDirectory = 'files';
    public $fileDirectoryPattern = 'file_{:id}'; 
    public $fileNamePattern = 'file_{:id}.{:extension}';
    
    public $file;

    public function rules()
    {
        return [
            // [['file'], 'required'],
            [['file'], 'file', 'maxSize'=> self::$MAX_FILE_SIZE_MB * 1024 * 1024],
            [['file'], 'file',  'mimeTypes' => $this->mimeTypes()]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'path' => 'Path',
            'file' => 'Content file',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function mimeTypes()
    {
        return [''];
    }

    public function fileType()
    {
        return 'generic';
    }

    public function fileName()
    {
        $fileName = str_replace('{:id}', $this->id, $this->fileNamePattern);
        $fileName = str_replace('{:extension}', $this->extension, $fileName);

        return $fileName;
    }
    
    public function paths(){

        $paths = [];

        $paths['self-directory'] = join(DIRECTORY_SEPARATOR,[
            Yii::getAlias('@app'),
            self::$FILE_CONTENTS_DIR,
            $this->parentDirectory,
            str_replace("{:id}", $this->id, $this->fileDirectoryPattern)
            ]);
        $paths['file'] = $paths['self-directory'].DIRECTORY_SEPARATOR.$this->fileName();
        $paths['stored-file'] = '/'.join('/',[self::$FILE_CONTENTS_DIR, $this->parentDirectory, str_replace("{:id}", $this->id, $this->fileDirectoryPattern), $this->fileName()]);

        return $paths;
    }
    
    public function pathTo($target = 'file')
    {
        $destinations = $this->paths();

        if($destinations[$target])
            return $destinations[$target];
        
        throw new yii\base\ErrorException("target '$target' is not defined");
    }

     public function storeFile()
    {
     	if(!$this->file)
     	{
     		return;
     	}
     	
        $selfDir = $this->pathTo('self-directory');
        if(!file_exists($selfDir))
        {
            if(!mkdir( $selfDir, 0775, true) || !chmod($selfDir, 0775))
                return FALSE;
        }
         
        //if successful saving
        //write the audio file to filesystem
        $filePath = $this->pathTo('file');

        if ($this->file->saveAs($filePath)) {
            Yii::info("Write file: " + $filePath);
            $this->file_path = $filePath;

            return TRUE;
        }

        return FALSE;
    }

     public function beforeValidate()
    {
        if($this->awareness_context && !$this->awareness_context->validate()){
            return false;
        }
        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }
    
    public function afterSave($insert, $changedAttributes)
    {
        // if($insert)
        // {
        //     $fileSaved = $this->storeFile();
        //     if(FALSE === $fileSaved){
        //         return;
        //     }
            
        //     $this->file_path = $this->pathTo('file');
        //     $this->save(false);
        // }

        parent::afterSave($insert, $changedAttributes);
    }
}