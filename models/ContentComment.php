<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;
use app\models\User;
use yii\helpers\Url;

class ContentComment extends ActiveRecord{


    public static function tableName(){
        return 'user_content_comment';
    }

    public function rules(){
        return [
            [['text'], 'string', 'min' =>3, 'max'=> 255],
            [['content_id'], 'exist', 'targetAttribute'=>'id', 'targetClass'=>'app\models\UserContent']
        ];
    }

    public function fields(){
        $fields = [
            'id',
            'text',
            'author' => function($model, $field){
                return User::findOne($model->author);
            },
            'created_at',
            'updated_at'
        ];

        if(Yii::$app->user->can('deleteComment', ['comment'=>$this])){
            
            $fields['links'] = function($model, $field){
                return ['delete' => Url::to(['content-comment/delete', 'id'=>$model->id])];
            };
        }
        // if($this->author == Yii::$app->user->getID()){
        //     $fields['links'] = function($model, $field){
        //         return ['delete' => Url::to(['content-comment/delete', 'id'=>$model->id])];
        //     };
        // }
        return $fields;
    }

}