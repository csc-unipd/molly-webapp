<?php 
namespace app\models;

use app\models\AwarenessContext;

class AwarenessFileContent extends FileContent
{
	
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(
            parent::attributeLabels(),
            [
                'awareness_context' => 'Awareness context',
            ]
        );
    }

    public function beforeSave($insert)
    {    	
    	
        if (parent::beforeSave($insert)) {
            if($this->awareness_context){
                if($insert){
                    if(!$this->awareness_context->save()) {
                            return false;         
                    }            
                }

                $id = $this->awareness_context->getPrimaryKey();
                $this->awareness_context = $id;
            }
            return true;
        } 
        
        return false;
    }


    public function afterSave($insert, $changedAttributes){
        if($this->awareness_context)
            $this->awareness_context = AwarenessContext::findOne($this->awareness_context);
            
        parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {   
        $this->awareness_context = AwarenessContext::findOne($this->awareness_context);
        parent::afterFind();
    }
}