<?php

namespace app\models;

use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "dossier".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 */
class Dossier extends \yii\db\ActiveRecord
{

    static $SCENARIO_CREATE_CONTENT = 'create-content';
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dossier';
    }
    

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
		    [['title'], 'string', 'skipOnEmpty'=>false, 'min'=>8, 'max'=>40],
            [['description'], 'string', 'min'=>10, 'max'=>1000],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['id'], 'safe', 'on'=>[self::$SCENARIO_CREATE_CONTENT]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function fields(){      
        $fields =[
                'creator' => function($dossier){
                return $dossier->getCreator();
            },
            'action_view'=>function($dossier){
                return Url::to(['dossier/view', 'id' => $this->id]);
            },
            'contents' => function($dossier){
                return $dossier->getContents();
            },
            'delete' => function($dossier){
                
                return Url::to(['dossier/delete', 'id'=>$dossier->id]);
            }
        ];
        $fields = array_merge($fields, parent::fields());
        return $fields;
    }

    public function getCreator(){
        return User::findOne($this->creator);
    }

    public function getContentsInternalDirectory(){

        return "/files/dossiers/$this->id";
    }

    public function getContents($beforeID = null){
        $maxContentsPerRequest = 5;

        $q = UserContent::find()
                ->innerJoin('dossier_content_map as map', "map.content = user_content.id")
                ->andWhere('map.dossier=:dossierID',[':dossierID'=>$this->id]);
        if(!empty($beforeID) && is_numeric($beforeID) && $beforeID > 0){
                $q->andWhere('user_content.id < :id', [':id'=>$beforeID]);
        }
               
        $contents = $q->orderBy('user_content.id DESC')
                ->limit($maxContentsPerRequest)
                ->all();

        if(count($contents) < $maxContentsPerRequest){
            $contents[] = null;
        }
        return $contents;
    }


    public function afterSave($insert, $changedAttributes){
        if($insert){
            //crea cartelle per i contenuti
            $baseDir = Yii::getAlias('@app').DIRECTORY_SEPARATOR.$this->getContentsInternalDirectory();
            if(!mkdir($baseDir, 0775)){
                throw new \yii\web\ServerErrorHttpException("Cannot create dossier directory");
            }

            if(!mkdir($baseDir.DIRECTORY_SEPARATOR.'audio', 0775)){
                throw new \yii\web\ServerErrorHttpException("Cannot create dossier audio directory");
            }

            if(!mkdir($baseDir.DIRECTORY_SEPARATOR.'video', 0775)){
                throw new \yii\web\ServerErrorHttpException("Cannot create video directory");
            }
            if(!mkdir($baseDir.DIRECTORY_SEPARATOR.'image', 0775)){
                throw new \yii\web\ServerErrorHttpException("Cannot create image directory");
            }
        }

        return parent::afterSave($insert, $changedAttributes);
    }
}
