<?php

namespace app\models;

use Yii\web\UploadedFile;
use Yii;
use yii\helpers\FileHelper;
use yii\validators\FileValidator;

class AwarenessContext extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'awareness_context';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['weather_temperature'], 'number', 'max'=>80, 'min'=>-40],
            [['weather_conditions'], 'each', 'rule'=>['integer']],
            [['weather_humidity'], 'integer', 'min'=> 0, 'max'=>100],
            [['location_latitude'], 'number', 'min'=> -90, 'max'=>90],
            [['location_longitude'], 'number', 'min'=> -180, 'max'=>180],
            [['location_altitude'], 'number', 'min'=> 0],
            [['location_accuracy'], 'number', 'min'=> 0],
            [['speed'], 'number', 'min'=> 0],
            [['headphone_plugged'], 'boolean'],
            [['activity_type'], 'integer'],
            [['activity_confidence'], 'integer', 'min'=>0, 'max'=>100]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
        ];
    }


    public function beforeSave($insert)
    {
        if(is_array($this->weather_conditions)){
            $this->weather_conditions = json_encode($this->weather_conditions);
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
         $this->decodeWeatherConditions();
        return parent::beforeSave($insert);
    }

    public function afterFind(){
        $this->decodeWeatherConditions();
        return parent::afterFind();
    }

    public function decodeWeatherConditions(){
        if(is_string($this->weather_conditions))
            $this->weather_conditions = json_decode($this->weather_conditions, true);
    }

    /**
    * @inheritdoc
    */
    public function formName(){
        return "AwarenessContext";
    }
}