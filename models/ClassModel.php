<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "class".
 *
 * @property int $id
 * @property int $creator
 * @property string $title
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 *
 * @property User $creator0
 * @property Dossier[] $dossiers
 */
class ClassModel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['creator', 'title', 'description'], 'required'],
            [['creator'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['creator'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'creator' => 'Creator',
            'title' => 'Title',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function fields(){

        return array_merge(parent::fields(), [
            'creator'=>function($class){
                return $class->getCreator();
            },

            'dossiers' => function($class){
                return $class->getDossiers()->all();
            }
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator'])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDossiers()
    {
        return $this->hasMany(Dossier::className(), ['class' => 'id']);
    }
}
