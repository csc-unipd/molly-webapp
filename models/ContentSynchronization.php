<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;
use app\models\AudioContent;


class ContentSynchronization extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content_synchronization';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content_ids' => 'Audio content ids to synchronize',
            'result' => 'Synchronization result',
            'completed_at' => 'Completion time',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['content_ids'], 'each', 'rule' => ['integer']],
            ['content_ids', 'safe']
        ];
    }

    public function run()
    {
            $binDirectory = join(DIRECTORY_SEPARATOR, [Yii::getAlias('@app'), 'bin']);
            $commandName = "java -jar ./bin/audioalign.jar";
            if(!chdir(Yii::getAlias('@app'))){
                return "error";
            }      

            //retrieve contents file paths
            $rows = (new \yii\db\Query())
                ->select(['file_path', 'type', 'id'])
                ->from('user_content')
                ->where(['id' => $this->content_ids])
                ->all();
            
            $filePaths = [];
            $referenceContentID = $this->content_ids[0];
            $referenceFilePath = "";

            foreach($rows as $row){
                if($row['id'] == $referenceContentID){
                    $referenceFilePath = $row['file_path'];
                }
            }
            $results = [];
            $fullOutput="";
            foreach($rows as $row){
                if($row['id'] == $referenceContentID){
                    continue;
                }
                // echo $referenceFilePath." - ". $row['file_path'];
                $cmd = join(' ', [$commandName, Yii::getAlias('@app').$referenceFilePath, Yii::getAlias('@app').$row['file_path']]);
                // echo "$cmd\n";
                $lines = "";
                $exitCode;
                
                $output = exec ($cmd, $lines, $exitCode);
                // echo $exitCode;        
                $fullOutput .= $output;
                if($exitCode == 0 && $cmdOutput = json_decode($output)){
                    //output is as expected
                    $message = $cmdOutput->message;
                    if($message->type == 'result'){
                        
                        $results [] = [
                            "content_id" => $row['id'],
                            "delay" => $message->delay
                        ];
                    }
                }
            }

            // die($fullOutput);
            $this->result = json_encode($results);
            $this->completed_at = date("Y-m-d H:i:s");;
            $this->save(false);
            return $results;
    }


    public function beforeSave($insert)
    {
        $this->content_ids = json_encode($this->content_ids);
        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        $this->content_ids = json_decode($this->content_ids);
        return parent::afterSave($insert, $changedAttributes);
    }

    public function afterFind()
    {
        $this->content_ids = json_decode($this->content_ids);
        return parent::afterFind();
    }


    public function getResultFor($id){

        $results = json_decode($this->result);
        foreach($results as $result)
        {
            if($result->content_id == $id){
                return $result;
            }
        }
        return null;
}
    
}