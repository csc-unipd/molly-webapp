## Progetto Molly ##

Il progretto sfrutta il framework php [Yii](http://www.yiiframework.com/). Viene sviluppato utilizzando un server http [nginx ](http://nginx.org), [php-fpm](https://php-fpm.org/) e un database sql [mariadb](https://mariadb.org/).

Viene testato con il browser Google Chrome.

### Guida installazione in breve ###

Cloniamo la repository

```
    #!bash
    git clone https://Bikappa@bitbucket.org/Bikappa/molly.git /path/to/project

```

in una cartella a scelta che chiameremo **/path/to/project**.

Per testare il progetto occorre disporre di un server lemp stack, (linux, nginx, mysql, php).
Fare riferimento a [questo link](https://www.digitalocean.com/community/tutorials/how-to-install-linux-nginx-mysql-php-lemp-stack-in-ubuntu-16-04) per la configurazione di un server lemp.
In generale:

```
#!bash


sudo apt-get update
#nginx
sudo apt-get install nginx

#mysql
sudo apt-get install mysql-server
sudo mysql_secure_installation

#php7.0-fpm
sudo apt-get install php-fpm php-common

#configuration steps...
#...


```

Controllare se il server web è installato correttamente aprendo il browser all'indirizzo http://localhost.

Creare un virtual host come indicato nella guida prendendo come modello il file "molly.conf" della repository modificando opportunamente le occorrenze di /path/to/project.

Il file di configurazione "molly.conf" andrà quindi inserito nella cartella /etc/nginx/sites-enabled dopodichè testare e riavviare nginx:


```
#!bash
sudo unlink /etc/nginx/sites-enabled/default 

sudo nginx -t
#if successfull
sudo nginx -s reload

```

Testare se php funziona correttamente collegandosi all'indirizzo http://localhost/testphp.php. 


Installiamo ora il package manager [composer](https://getcomposer.org/):

```
#!bash

sudo curl -sS https://getcomposer.org/installer | php
mv composer.phar /usr/local/bin/composer
composer global require "fxp/composer-asset-plugin:^1.2.0"
```

Spostiamoci nella cartella /path/to/project e installiamo tutte le dipendenze necessarie sfruttando composer:


```
#!bash

composer install
```

Installiamo il software ffmpeg globalmente secondo questa [guida](https://trac.ffmpeg.org/wiki/CompilationGuide/Ubuntu) (opzione yasm)

A questo punto creiamo il database "molly" sul server mysql, l'utente "molly" con password "yllom" e forniamo a questo i permessi di operare sul database "molly"


```
#!mysql
mysql -p -u root

CREATE USER 'molly'@'localhost' IDENTIFIED BY 'yllom';
GRANT ALL PRIVILEGES ON molly.* TO 'molly'@'localhost';

exit

```


Per creare la struttura del database usare il file "molly.sql" contenuto nella repository. Verranno inseriti nel database "molly" un utente con email "mario.rossi@gmail.com" e password "fazzoletti" nonché un unico dossier sul quale è possibile operare.


**Importante: Dare i permessi ad nginx di accedere e scrivere i file nella cartella del progetto**
Esempio:

```
#!bash
#luca is the user actually owning the project folder
#www-data is the default group used by nginx
chown luca:www-data /path/to/project -R
chmod 775 /path/to/project -R

```

Ora dovremmo poter raggiungere il server, dopo aver avviato nginx, php-fpm e mysql/mariadb, all'indirizzo 127.0.0.1