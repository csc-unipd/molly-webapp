
var AwarenessSnapshot = function(config)
{
     var snapshotProvider = null; 
     var onTakeSnapshot = null;   
     var onSnapshotTaken = null;

     if(config != null && 'snapshotProvider' in config && typeof config.snapshotProvider === 'function')
         snapshotProvider = config.snapshotProvider;
     if(config != null && 'onSnapshotTaken' in config && typeof config.onSnapshotTaken === 'function')
         onSnapshotTaken = config.onSnapshotTaken;
     if(config != null && 'onTakeSnapshot' in config && typeof config.onTakeSnapshot === 'function')
         onTakeSnapshot = config.onTakeSnapshot;
      
     this.weather_temperature = null;
     this.weather_conditions = null;
     this.weather_humidity = null;
     this.location_latitude = null;
     this.location_longitude = null;
     this.location_altitude = null;
     this.location_accuracy = null;
     this.speed = null;
     this.headphone_plugged = null;
     this.activity_type = null;
     this.activity_confidence = null;

     var that = this;
AwarenessSnapshot.prototype.takeSnapshot = function ()
    {
        
        if(!snapshotProvider){
            console.error("You must set a snaphotProvider");
        }
        var snapshot = this;
        if(onTakeSnapshot != null)
        {
            onTakeSnapshot();
        }
        
        snapshotProvider(function(snapshotData){
            snapshot.snapshotTaken(snapshotData);
        });      
    };   


AwarenessSnapshot.prototype.fromJson = function(jsonString)
    {
        if(!jsonString || !typeof jsonString === "string")
            {
                return false;
            }
            var data = JSON.parse(jsonString);

            if(data)
            {
                for(var propertyName in data)
                    {
                        if(that.hasOwnProperty(propertyName)){                      
                            that[propertyName] = data[propertyName];                   
                        };
                    }                    

                
            }
            console.log(that);
    };

    AwarenessSnapshot.prototype.snapshotTaken = function(jsonSnapshot)
    {
        console.log("Snapshot data:");
        console.log(jsonSnapshot);

        this.fromJson(jsonSnapshot);

        if(onSnapshotTaken != null)
        {
            onSnapshotTaken();
        }
    }
}