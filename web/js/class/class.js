

var ClassViewer = function (classModel){
    this.classModel = classModel;
    console.log(classModel);
    this.init();
}


ClassViewer.prototype ={

    init: function(){

         $(".date-time").each(function(){
            var gmtDate = $(this).data('time');
            var m  = moment(gmtDate, 'YYYY-MM-DD hh:mm:ss').subtract(window.timeZoneOffset, 'minutes');
            $(this).text(m.format("ll"));
        });

        var template = $('#dossier-list-template').html();
        Mustache.parse(template);
        var rendered = Mustache.render(template, this.classModel);
        $('#dossiers-list').append(rendered);

        var dossiers = this.classModel.dossiers;
        console.log(dossiers);
        
        if(!dossiers || dossiers.length == 0  || dossiers[0]==null){
            $("#no-contents").show();
        }
    }
}
