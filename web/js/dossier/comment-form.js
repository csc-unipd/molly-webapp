jQuery.fn.extend({
    commentForm: function (onCommentSaved) {

        return this.each(function () {
            var self = this;

            $(this).submit(function (e) {
                e.preventDefault();
                var data = $(this).serialize();
                console.log("Sending comment: " + data);
                self.onCommentSaved = onCommentSaved;
                var url = this.action;
                $.post({
                    url: url,
                    data: data,
                    dataType: 'json',
                }).success(function (savedComment) {
                    console.log(savedComment);
                    if (self.onCommentSaved && typeof self.onCommentSaved == 'function') {

                        self.onCommentSaved(savedComment)
                    }
                }).fail(function (response) {
                    console.log(response);
                    if (response.status == 422) {
                        //message has errors
                        var errors = response.responseJSON;
                        showErrors(self, errors);
                    }
                });
                this.reset();
                return false;
            });

            var showErrors = function (form, errors) {
                Object.keys(errors).forEach(function (key, index) {
                    console.log(key);
                    var formGroup = $(form).find("[name='" + key + "']").closest('.form-group');
                    formGroup.addClass('has-warning');
                    var feedback = formGroup.find(".form-control-feedback");
                    feedback.show();
                    feedback.text(errors[key]);
                });
            }
        });
    }
});