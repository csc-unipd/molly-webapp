function DossierViewer(dossier) {
    console.log(dossier);
    var thisViewer = this;
    this.dossierID = dossier.id;
    this.tmplScriptSelector = '#content-item-template';
    this.awarenessContextTmpl = '#awareness-context-template';
    this.contentsContainer = $('#dossier-contents-container');
    this.contentsList = this.contentsContainer.find(".contents-list");
    this.contents = [];
    this.loaderElement = this.contentsList.find(".loader");
    this.init();

    //this.loadContents(this.loadedContentsType);
    this.onContentsLoaded(dossier.contents);
};

DossierViewer.prototype = {
    selectors: {
        contentItem: ".content-item",
        selectTypeMenuBtn: '#contentTypesMenu',
        selectTypeMenuList: '.dropdown-menu',
        itemSynchronizeSelect: '.js-synchronize-select-content',
        itemDeleteBtn: '.js-delete-content',
        itemPlayingStatus: '.playing-status',
        itemTimeProgress: '.time-progress',
        itemTimeProgressBar: '.bar',
        synchronizeBtn: '.synchronize-btn',
        currentTypeIndicator: '.current-content-type',
        toSynchronizeContentsCounter: '.selected-counter',
        itemPlayer: '.item-player',
    },
    init: function () {
        this.bindInterface();
        this.itemTemplate = this.getContentItemTemplate(this.tmplScriptSelector);
        this.commentTemplate =$('#comment-template').html();
        Mustache.parse(this.commentTemplate);
        
        this.awarenessContextTemplate = $(this.awarenessContextTmpl).html();
        var thisViewer = this;
        this.contentSynchronizer = new ContentSynchronizer(this.dossierID);
        this.contentSynchronizer.onSelectionChanged = function (contentID) {
             console.log(contentID);
            var selected = this.isSelected(contentID);

            var item = thisViewer.getContentItem(contentID);
            item.find(thisViewer.selectors.itemSynchronizeSelect).toggleClass('selected');
            console.log(thisViewer.contentSynchronizer.selectedContents);

            var nSelectItems = thisViewer.contentSynchronizer.selectedContents.length
            var countBadge = $(thisViewer.selectors.toSynchronizeContentsCounter);

            countBadge.html(nSelectItems + " Contents");
            switch (nSelectItems) {
                case 0:
                    countBadge.addClass('hidden');
                    break;
                case 1:
                    countBadge.removeClass('hidden');
                    countBadge.parent().addClass('disabled');
                    break;
                default:
                    countBadge.parent().removeClass('disabled');
                    break;
            }
        };

        $(document).on('contents-rendered', function(){
            thisViewer.onContentsListRendered();
        });
    },

    bindInterface: function () {
        var viewer = this;
        var ss = this.selectors;
        var typesList = this.contentsContainer.find(ss.selectTypeMenuBtn + "+" + ss.selectTypeMenuList);
        console.log(typesList);
        typesList.find("a").click(function (e) {
            var contentTypeToShow = $(e.target).attr('href').substring(1);
            viewer.loadContents(contentTypeToShow);
        });
        console.log($(ss.synchronizeBtn));
        $(ss.synchronizeBtn).click(function () {
            viewer.contentSynchronizer.launch();
        });

         this.contentsList.on('click', '.play', function (event) {
            viewer.onPlayButtonClicked(event);
        });

        this.contentsList.on('click', ss.itemSynchronizeSelect, function (event) {
            event.preventDefault();
            viewer.onSynchronizeSelectClicked(event)
        });
        this.contentsList.next('.show-more').on('click',function(){
            viewer.loadContents();
        });
        this.contentsList.on('click',ss.itemDeleteBtn, function (event) {
            
            var contentToDelete = viewer.getContentByID($(event.target).data('content-id'));
            console.log("Content to delete: "+$(event.target).data('content-id'));
            console.log(contentToDelete);

            var deleteModal = $('#delete-modal');
            deleteModal.on('show.bs.modal', function (event) {
                var modalTemplate = $('#delete-modal').html();
                var modalContent = Mustache.render(modalTemplate, contentToDelete);
                deleteModal.html(modalContent);

                deleteModal.on('hidden.bs.modal', function(){
                    deleteModal.html(modalTemplate);
                });
            });

            deleteModal.modal();     
        });

        $(document).on('click', function (e) {
            $('[data-toggle="popover"],[data-original-title]').each(function () {
                //the 'is' for buttons that trigger popups
                //the 'has' for icons within a button that triggers a popup
                if (!$(this).is(e.target) && $(this).has(e.target).length === 0 && $('.popover').has(e.target).length === 0) {                
                    (($(this).popover('hide').data('bs.popover')||{}).inState||{}).click = false  // fix for BS 3.3.6
                }

            });
        });

        // this.contentsList.click(ss.itemTimeProgress, function (evt) {
        //     var contentItem = thisViewer.getElementContainerItem(evt.target);
        //     $mediaElem = contentItem.find("audio, video");

        //     var clickX = evt.pageX - $(evt.target).offset().left;
        //     seekPosRelative = clickX / $(evt.target).width();
        //     duration = $mediaElem[0].duration;
        //     seekPos = duration * seekPosRelative;
        //     console.log(seekPos);
        //     $mediaElem[0].currentTime = seekPos;
        // });

    },

    getContentItemTemplate: function (tmplScriptSelector) {

        if (typeof tmplScriptSelector !== 'string') {
            console.err('tmplScriptSelector must be a string');
            return null;
        }

        var template = $(tmplScriptSelector).html();
        Mustache.parse(template);
        return template;
    },

    loadContents: function (type) {

        // if (typeof type !== 'string') {
        //     console.err("Type of type must be a string: " + typeof type + " given");
        //     return false;
        // }

        if(this.contents[this.contents.length-1] == null){
            return;
        }

        this.onContentsLoadBegin();
        var lastID = this.contents[this.contents.length-1].id;

        var url = '/dossier/contents?id=' + this.dossierID + '&before_id=' + lastID;
        var viewer = this;

        $.ajax({
            url: url,
            dataType: 'json',
            method: 'GET',
            success: function (rawContents) {
                
                viewer.onContentsLoaded(rawContents);
            },
            complete: function (response) {
                //check if result if successfull
                if (response.status != 200) {
                    console.error(response);
                }
            }
        });

        return true;
    },

    onContentsLoadBegin: function () {

        // //update current content type info
        // var contentTypesList = $(this.selectors.selectTypeMenuList);
        // var currentTypeHtml = contentTypesList.find("a[href='#" + this.loadedContentsType + "']").html();
        // console.log(contentTypesList);
        // console.log(contentTypesList.find("a[href='#" + this.loadedContentsType + "']"));

        // $(this.selectors.currentTypeIndicator).html(currentTypeHtml);
        //  console.log($(this.selectors.currentTypeIndicator));

        //erase contents list
        //this.contentsList.empty();

        //show the loader
        this.contentsList.append(this.loaderElement);
        this.loaderElement.show();
    },

    onContentsLoaded: function (rawContents) {
        console.log(rawContents);
        var userContents = [];
        var last = false;
        if(rawContents[rawContents.length-1] == null){
            //theese are the last contents
            last = true;
        }
        rawContents.forEach(function (rawContent,index) {
            if(index == rawContents.length - 1 && last){
                console.log("Last content loaded");
                userContents.push(null);
                return;
            } 
            userContents.push(new UserContent(rawContent));
        });

        if(last){
            this.contentsList.next('.show-more').hide();
            
        }else{
            this.contentsList.next('.show-more').show();
        }
        this.contents = this.contents.concat(userContents);

        if(this.contents.length == 0 || this.contents[0] == null){
            $("#no-contents").show();
        }
        this.renderContentsList();
    },

    onContentsListRendered: function () {
        var thisViewer = this;
        var ss = this.selectors;
        
        // this.contentsList.find(this.selectors.contentItem).each(function(){
        //     thisViewer.loadItem($(this));
        // });

        // this.contentsList.find('.play').click(function (event) {
        //     thisViewer.onPlayButtonClicked(event);
        // });

        // var mediaElements = this.contentsList.find("audio, video");
        // mediaElements.mediaelementplayer({
        //     alwaysShowControls: true,
        //     defaultAudioWidth: "100%"
        // });
        
        // var playingListener = function (event) {
        //     var contentItem = thisViewer.getElementContainerItem(event.target);
        //     contentItem.find(ss.itemPlayingStatus).toggleClass('playing');
        // }
        // mediaElements.on('playing', playingListener);

        // mediaElements.on('pause', playingListener);

        // mediaElements.on('timeupdate', function (event) {
        //     var duration = event.target.duration;
        //     var currentTime = event.target.currentTime;
        //     progress = currentTime / duration;
        //     var contentItem = thisViewer.getElementContainerItem(event.target);
        //     var progressBar = contentItem.find(ss.itemTimeProgress + " " + ss.itemTimeProgressBar);
        //     progressBar.css('width', progress * 100 + "%");
        //     contentItem.find('span[name=\'duration\'] + span').html(formatTime(currentTime) + " | " + formatTime(duration));
        // });

        //  $('.comment-controller').commentController(this.commentTemplate);       
    },

    getContentByID: function(id){
        var that = this;
        var wanted = null;
        for(var i = 0; i< that.contents.length; ++i){
            var content = that.contents[i];
            if(content.id == id){
                wanted = content;
                break;
            }
        }

        return wanted;
    },

    onSynchronizeSelectClicked: function (event) {

        var contentID = $(event.target).data('content-id');
        this.contentSynchronizer.toggleContent(contentID);
    },

    renderContentsList: function () {

        if(this.contents.length == 0){
            this.contentsList.html("No contents");
        }
        this.loaderElement.remove();
        var viewer = this;
        this.contents.forEach(function (content, index) {
            if(content == null){
                return;
            }
            if(content.rendered){
                //content already rendered
                return true;
            }

            var listItem = Mustache.render(viewer.itemTemplate, content);
            listItem = $(listItem);
            listItem[0].content = content;
            viewer.contentsList.append(listItem);
            content.rendered = true;

            var mediaElements = listItem.find("audio, video");
            mediaElements.mediaelementplayer({
                alwaysShowControls: true,
                defaultAudioWidth: "100%"
            });

            listItem.find('.comment-controller').commentController(viewer.commentTemplate);
            viewer.loadItem(listItem); 
        });

        $(document).trigger('contents-rendered');
    },

    onPlayButtonClicked: function (event) {
        var theButton = $(event.target);
        var contentItem = $(theButton).closest(this.selectors.contentItem).first();
        var contentID = contentItem.data('content-id');
        this.playContent(contentID);
    },

    playContent: function (contentID) {
        var contentItem = $(this.selectors.contentItem + '[data-content-id=\'' + contentID + '\']');
        var contentType = contentItem.data('content-type');
        var mediaElemSelectorMap = {
            audio: 'audio',
            video: 'video'
        }

        var mediaElem = contentItem.find(mediaElemSelectorMap[contentType]);
        if (mediaElem.get(0).paused) {
            mediaElem[0].play();
        } else {
            mediaElem[0].pause();
        }

    },

    loadItem: function(item){
        var content = item[0].content;
        if (content.isAudio()) {
            var loader = this.loaderElement;
            item.find(this.selectors.itemPlayer).prepend(loader);
            this.loadWaveform(content);

        }

        if (content.awareness_context) {
            
                console.log(content.awareness_context);
                var awarenessContext = new AwarenessContext(content);  
                var rendered = Mustache.render(this.awarenessContextTemplate, awarenessContext);
                item.find('.awareness-context').append(rendered);
                
            }
    },   

    onWaveformLoaded: function (content) {
        var item = this.getContentItem(content.id);
        item.find(this.selectors.itemPlayer + " .loader").remove();
    },

    getElementContainerItem: function (element) {
        return $(element).closest(this.selectors.contentItem);
    },

    getContentItem: function (contentID) {
        return $(this.selectors.contentItem + '[data-content-id=\'' + contentID + '\']');
    },

    loadWaveform: function (content) {
        var contentItem = this.getContentItem(content.id);
        var audioElement = contentItem.find('audio');
       // console.log('Waveform uri: ' + audioElement.data('waveformUri'));

        //init configuration for peaks.js
        var peaksOptions = {
            container: contentItem.find('.peaks-container').get(0),
            mediaElement: audioElement.get(0),

            /** Optional config with defaults **/
            // URI to waveform data file in binary or JSON
            dataUri: {
                // arraybuffer: '../test_data/sample.dat',
                // json: '/sample.json'// audioElement.data('waveform-uri')
                // json:  audioElement.data('waveform-uri')
            },

            // audioContext: new AudioContext(),
            // async logging function
            logger: console.error.bind(console),

            // default height of the waveform canvases in pixels
            height: 150,

            // Array of zoom levels in samples per pixel (big >> small)
            zoomLevels: [512, 1024, 2048, 4096],

            // Bind keyboard controls
            keyboard: false,

            // Keyboard nudge increment in seconds (left arrow/right arrow)
            nudgeIncrement: 0.01,

            // Colour for the in marker of segments
            inMarkerColor: '#a0a0a0',

            // Colour for the out marker of segments
            outMarkerColor: '#a0a0a0',

            // Colour for the zoomed in waveform
            zoomWaveformColor: 'rgba(0, 225, 128, 1)',

            // Colour for the overview waveform
            overviewWaveformColor: 'rgba(255,255,255,0.2)',

            // Colour for the overview waveform rectangle that shows what the zoom view shows
            overviewHighlightRectangleColor: 'rgba(200,200,200, 0.3)',

            // Colour for segments on the waveform
            segmentColor: 'rgba(255, 161, 39, 1)',

            // Colour of the play head
            playheadColor: 'rgba(255, 255, 255, 0.7)',

            // Colour of the play head text
            playheadTextColor: '#aaa',

            // the color of a point marker
            pointMarkerColor: '#FF0000',

            // Colour of the axis gridlines
            axisGridlineColor: '#ccc',

            // Colour of the axis labels
            axisLabelColor: '#aaa',

            // Random colour per segment (overrides segmentColor)
            randomizeSegmentColor: true,

            // Zoom view adapter to use. Valid adapters are: 'animated' (default) and 'static'
            zoomAdapter: 'animated',

            scale: 100
        }



        //get waveform data from the server

        var that = this;
        $.ajax({
            method: 'GET',
            url: audioElement.data('waveform-uri'),
            dataType: "json"
        }).done(function(data){
           // console.log("Visualizza waveform");
                var waveformData = data;
                peaksOptions['waveformData'] = waveformData;
                var spp = waveformData.samples_per_pixel;
                peaksOptions['zoomLevels'] = [spp, spp*2, spp*4, spp*8];
                // peaksOptions['scale']
                window.peaks.init(peaksOptions);
                that.onWaveformLoaded(content);
        }).fail(function(response){
            console.error(response);
        });

    }
}

function UserContent(rawData) {
    this.init(rawData);
}

UserContent.prototype = {
    init: function (rawData) {
        Object.defineProperties(this, Object.getOwnPropertyDescriptors(rawData));
    },
    isVideo: function () {
        return this.type === 'video'
    },
    isAudio: function () {
        return this.type === 'audio'
    },
    isImage: function () {
        return this.type === 'image'
    },

    isSynchronizable: function(){
        return this.isAudio() || this.isVideo();
    },
    rendered: false

}


function ContentSynchronizer() {
    this.selectedContents = [];
};

ContentSynchronizer.prototype = {
    addContent: function (contentID) {
        var contentIndex = this.selectedContents.indexOf(contentID);
        if (contentIndex == -1) {
            this.selectedContents.push(contentID);
            this.onSelectionChanged(contentID);
        }

        return contentIndex == -1;
    },

    removeContent: function (contentID) {
        var contentIndex = this.selectedContents.indexOf(contentID);
        if (contentIndex != -1) {
            this.selectedContents.splice(contentIndex, 1);
            this.onSelectionChanged(contentID);
        }
        return contentIndex != -1;
    },

    toggleContent: function (contentID) {
        if (!this.addContent(contentID)) {
            this.removeContent(contentID);
        }
    },

    isSelected: function (contentID) {
        return this.selectedContents.indexOf(contentID) !== -1;
    },

    onSelectionChanged: function () {
        //this will be overriten
    },

    showMessageModal: function(msg)
    {
       $modal = $("#message-modal");
       $modal.find('.message').html(msg);
       $modal.modal();
    },
    launch: function () {

        var contentIds = this.selectedContents;
        if (contentIds.length < 2) {
            this.showMessageModal("Select at least two contents to synchronize");
            return {
                error: "Select at least two contents to synchronize"
            };
        };

        var url = "/content-synchronization/new";
        var data = {
            ContentSynchronization: {
                content_ids: contentIds
            }
        };

        $.ajax({
            url: url,
            type: 'POST',
            data: data
        }).fail(function (jqXHR, textStatus, errorThrown) {
            if (jqXHR.status != 302)
                alert("An error occurred synchronizing contents");
            console.log(jqXHR);
        }).done(function (data) {
            console.log(data);
        }).always(function () {
            $('.synchronize-btn').toggleClass("on-progress");
        });

        console.log("request sent");
        $('.synchronize-btn').toggleClass("on-progress");
    }
}

function AwarenessContext(content) {
    
    this.load(content);
}

AwarenessContext.prototype = {
    //from google awareness api documentation
    activityNames: {
        0: 'in vehicle',
        1: 'on bicycle',
        2: 'on foot',
        8: 'running',
        3: 'still',
        5: 'tilting',
        4: 'unknown',
        7: 'walking',
    },
    weatherConditionNames: {
        1: 'clear',
        2: 'cloudy',
        3: 'foggy',
        4: 'hazy',
        5: 'icy',
        6: 'rainy',
        7: 'snowy',
        8: 'stormy',
        0: 'unknown',
        9: 'windy',
    },
    load: function (content) {
            var that = this;
            Object.defineProperties(that, Object.getOwnPropertyDescriptors(content.awareness_context));
            that.activity_type_text = that.activityNames[that.activity_type];
            that.weather_conditions_text = [];
            if (that.weather_conditions) {

                that.weather_conditions.forEach(function (val, index) {
                    that.weather_conditions_text.push(that.weatherConditionNames[val]);
                });
            };

    },
}

formatTime = function (timeInSeconds) {

    var time = timeInSeconds;
    var timeString = "";

    var ss = parseInt(time % 60);
    time = parseInt(time / 60);
    var mm = time % 60;
    time = parseInt(time / 60);
    var hh = time % 60;
    timeString = mm + ":" + ss;
    timeString = (hh > 0 ? hh : "") + timeString;
    return timeString;
};