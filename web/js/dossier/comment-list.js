jQuery.fn.extend({
    commentController: function(template){
        this.each(function(){
            var commentList = $(this).find('.comment-list');

           // console.log(commentList);
            commentList.commentList(template);
            var commentForm = $(this).find('.comment-form');
            commentForm.commentForm(function(savedComment){
                commentList.get(0).addNewComment(savedComment);
            });
        });
    },
    commentList: function(template){

        this.each(function(){
            var self = this;
            self.firstLoad = true;
            self.firstCommentDate = null;
            self.allCommentsLoaded = false;
            self.comments = [];
            self.commentsShown = 0;
            self.initialContentShown = 2;
            self.showMoreBtn = $(self).prev('.show-more');
            console.log("SHOW MORE BTNS");
            console.log(self.showMoreBtn);
            self.showMoreBtn.click(function(){
                self.showPreviousComments(true);
            });

            self.renderComment = function(comment){
                var rendered = Mustache.render(template, comment);
                return $(rendered);
            }
            self.prependComment = function(comment){
                
                
                 $(self).prepend( self.renderComment(comment));
            };

            self.addNewComment = function(comment){
                self.comments.push(comment);
                $(self).append( self.renderComment(comment));
                self.commentsShown++;
            };

            self.showPreviousComments = function(more){

                if(!more){
                    var start = self.comments.length - 1 - self.commentsShown;

                    var howMany = Math.min(self.initialContentShown, self.comments.length);

                    for(var i = start; i > start-howMany; --i){
                        self.prependComment(self.comments[i]);
                        self.commentsShown++;
                    }
                }else if(self.commentsShown < self.comments.length){

                        for(var i = self.comments.length - 1 - self.commentsShown; i >= 0; --i){
                            self.prependComment(self.comments[i]);
                            self.commentsShown++;
                        }
                }else if(!self.allCommentsLoaded){
                    self.getComments();
                }

                if(self.allCommentsLoaded && self.commentsShown == self.comments.length){
                            self.showMoreBtn.hide();
                        }     
            };

            self.addPreviousComment = function(comment){ 
                if(comment.created_at < self.firstCommentDate || !self.firstCommentDate){
                    self.comments.unshift(comment);
                    self.firstCommentDate = comment.created_at;
                    console.log( self.firstCommentDate);
                }
            }

            self.removeCommentByID = function(id){

                self.comments.forEach(function(comment, index){
                    if(comment.id == id){
                        console.log("found");
                        self.comments.splice(index, 1);
                        self.commentsShown--;
                        $(self).find(".comment[data-comment-id='"+ id +"']").remove();
                        return false;
                    }
                });
            }

            self.getComments = function(){
                var url = $(self).data('url');

                if(self.firstCommentDate){
                    url = url+'&before_date='+self.firstCommentDate;
                }
                $.ajax({
                    url: url,
                    type: 'GET'
                }).success(function(comments){
                    self.firstLoad = self.comments.length == 0;

                    if(comments[comments.length-1] == null){
                        self.allCommentsLoaded = true;
                    }


                    comments.forEach(function(comment){
                        if(comment)
                            self.addPreviousComment(comment);
                    });
                                
                    self.showPreviousComments(!self.firstLoad);
                                     
                }).fail(function(response){
                    console.log(response);
                });
            };

            $(self).on('click', '.delete-comment', function(e){
                e.preventDefault();

                var url = this.href;
                var cid = $(this).closest(".comment").data('comment-id');
                console.log(cid);
                $.ajax({
                    type: 'POST',
                    url: url
                }).success(function(){
                    console.log('comment deleted');
                    self.removeCommentByID(cid);

                }).fail(function(response){
                    console.error(response);
                });

                return false;
            }); 

            self.getComments();
            
        });
    }
});