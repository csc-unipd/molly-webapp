$(document).ready(function(){
    $.material.init();
   
    window.timeZoneOffset = (new Date()).getTimezoneOffset();
    window.updateTimes = function(){
        $(".relative-time").each(function(){
            var gmtDate = $(this).data('time');
            var m  = moment(gmtDate, 'YYYY-MM-DD hh:mm:ss').subtract(window.timeZoneOffset, 'minutes');
            $(this).text(m.fromNow());
        });

         $(".date-time").each(function(){
      
        var gmtDate = $(this).data('time');   
        var m  = moment(gmtDate, 'YYYY-MM-DD hh:mm:ss').subtract(window.timeZoneOffset, 'minutes');
        $(this).html(m.format("ll"));
    });   
    }
     
    

    window.updateTimes();
    window.setInterval(function(){
        window.updateTimes();
    }, 1000);
});
