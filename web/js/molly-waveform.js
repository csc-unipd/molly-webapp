(function () {

    var audioElement = $('audio');
    console.log('Waveform uri: '+ audioElement.data('waveformUri'));

    $('.peaks-container').html('Loading waveform data...');

    var peaksOptions = {
        container: $('.peaks-container').get(0),
        mediaElement: audioElement.get(0),

        /** Optional config with defaults **/
        // URI to waveform data file in binary or JSON
        dataUri: {
            // arraybuffer: '../test_data/sample.dat',
            // json: '/sample.json'// audioElement.data('waveform-uri')
            // json:  audioElement.data('waveform-uri')
        },

        // audioContext: new AudioContext(),
        // async logging function
        logger: console.error.bind(console),

        // default height of the waveform canvases in pixels
        height: 200,

        // Array of zoom levels in samples per pixel (big >> small)
        zoomLevels: [512, 1024, 2048, 4096],

        // Bind keyboard controls
        keyboard: false,

        // Keyboard nudge increment in seconds (left arrow/right arrow)
        nudgeIncrement: 0.01,

        // Colour for the in marker of segments
        inMarkerColor: '#a0a0a0',

        // Colour for the out marker of segments
        outMarkerColor: '#a0a0a0',

        // Colour for the zoomed in waveform
        zoomWaveformColor: 'rgba(0, 225, 128, 1)',

        // Colour for the overview waveform
        overviewWaveformColor: 'rgba(0,0,0,0.2)',

        // Colour for the overview waveform rectangle that shows what the zoom view shows
        overviewHighlightRectangleColor: 'rgba(200,200,200, 0.3)',

        // Colour for segments on the waveform
        segmentColor: 'rgba(255, 161, 39, 1)',

        // Colour of the play head
        playheadColor: 'rgba(0, 0, 0, 1)',

        // Colour of the play head text
        playheadTextColor: '#aaa',

        // the color of a point marker
        pointMarkerColor: '#FF0000',

        // Colour of the axis gridlines
        axisGridlineColor: '#ccc',

        // Colour of the axis labels
        axisLabelColor: '#aaa',

        // Random colour per segment (overrides segmentColor)
        randomizeSegmentColor: true,

        // Zoom view adapter to use. Valid adapters are: 'animated' (default) and 'static'
        zoomAdapter: 'animated'

    }

    $.ajax({
        method: 'GET',
        url: audioElement.data('waveform-uri'),
        dataType: 'json',
        success: function(data, status){
            console.log(data);
            var waveformData = data;
            peaksOptions['waveformData'] =  waveformData;
            window.peaks.init(peaksOptions);
        },
        error: function(response, status){
            console.log(response);
            if(response.status == 404 ){
                if(response.responseJSON && response.responseJSON.message){
                    $('.peaks-container').html(response.responseJSON.message);
                }else{
                    $('.peaks-container').html('Cannot load waveform');
                }
            }
        }

    });
    
    $('.btn-play').click(function () {

        if (audioElement.get(0).ended) {
            console.log('Audio is ended so i restart it from begin');
            audioElement.trigger('pause');
            audioElement.get(0).currentTime = 0;
        }

        audioElement.trigger('play');
    });

    $('.btn-pause').click(function () {
        audioElement.trigger('pause');
    });

})();