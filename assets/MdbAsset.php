<?php
namespace app\assets;

use yii\web\AssetBundle;

class MdbAsset extends AssetBundle{

    public $sourcePath = "@bower/bootstrap-material-design/dist";
    public $css = [
        'css/bootstrap-material-design.css',
        'css/ripples.css',
        'http://fonts.googleapis.com/icon?family=Material+Icons'
    ];
    public $js = [
        'js/material.js',
        'js/ripples.js',
    ];
}

