<?php
namespace app\assets;

use yii\web\AssetBundle;

class MomentAsset extends AssetBundle{

public $sourcePath = '@npm';

    public $js = [
        'moment/moment.js'
    ];

}