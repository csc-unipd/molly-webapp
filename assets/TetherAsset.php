<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TetherAsset extends AssetBundle
{
    public $sourcePath  = '@bower/tether/dist';
    public $css = [
        'css/tether.css'
    ];
    public $js = [
        'js/tether.js'
    ];
    
}
