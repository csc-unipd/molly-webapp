<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MustacheAsset extends AssetBundle
{
    public $sourcePath  = '@npm/mustache';
    public $js = [
        'mustache.js'
    ];
}
