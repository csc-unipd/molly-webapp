<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
        'css/molly.less',
        'css/dossier.less'
    ];

    public $js =[
        'js/dossier/dossier.v2.js',
        'js/dossier/comment-form.js',
        'js/dossier/comment-list.js',
        'js/common.js',
    ];
    
    public $depends = [
        'yii\web\YiiAsset',
        'app\assets\MustacheAsset',
        'app\assets\MomentAsset',
        'app\assets\BootstrapAsset',
        'app\assets\MdbAsset',
    ];
}
