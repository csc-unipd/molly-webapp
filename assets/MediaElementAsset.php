<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class MediaElementAsset extends AssetBundle
{
    public $sourcePath  = '@npm/mediaelement/build';
    public $js = [
        'mediaelement-and-player.min.js'
    ];

    public $css =[
        "mediaelementplayer.min.css"
    ];
}
