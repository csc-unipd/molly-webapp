<?php
namespace app\assets;

use yii\web\AssetBundle;

class DossierAsset extends AssetBundle{


   public $depends = [
        'app\assets\AppAsset',
        'app\assets\MediaElementAsset',
    ];
}