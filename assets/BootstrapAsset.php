<?php
namespace app\assets;

use yii\web\AssetBundle;

class BootstrapAsset extends AssetBundle{

 
    public $sourcePath = "@vendor/twbs/bootstrap/dist";
    public $css = [
        'css/bootstrap.css',
        'css/bootstrap-grid.css',
        'css/bootstrap-reboot.css'
    ];
    public $js = [
        'js/bootstrap.js'
    ];

    public $depends =
    [
        'app\assets\TetherAsset'
    ];
}

