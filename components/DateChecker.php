<?php

namespace app\components;

use Yii;
use yii\base\Component;

class DateChecker extends Component
{

    public function isDate($dateString)
    {
        return date('Y-m-d H:i:s', strtotime($dateString)) == $dateString;
    }
}