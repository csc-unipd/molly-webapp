<?php

namespace app\components;

use Yii;
use yii\base\Component;

class OSHelper extends Component
{

    public function isWindows()
    {
        return $isWindows = substr(PHP_OS,0,3) === 'WIN';
    }
}