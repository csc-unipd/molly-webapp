<?php

$params = require(__DIR__ . '/params.php');
$commonConfig = require(__DIR__.'/common.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [

            'parsers'=>[
                'application/json'=> 'yii\web\JsonParser'
            ],
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'Yre0tGgk1WaSgMyVONT-x8vf20_mTROV',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning', 'info'],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['info'],
                    'logFile' => "@runtime/logs/info.log"
                ],
            ],
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            	'user-content/<type:\w+>/<action:\w+>'=> 'user-content/<action>',
            	'user-content/<type:\w+>/<id:\d+>/<action:\w+>'=> 'user-content/<action>',
                'dossier/contents/<type:\w+>'=> 'dossier/contents',
                '<controller:w+>/<id:\d+>/<action:\w+>'=> '<controller>/<action>',
                '<controller:w+>/<id:\d+>' => '<controller>/<view>',             
                '<controller:w+>/<action:w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:w+>/<action:w+>' => '<controller>/<action>',
            ],
        ],
        'response' =>[
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // use "pretty" output in debug mode
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                    // ...
                ],
            ],
        ],       
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => [ '*']
    ];
    
}

return array_merge_recursive($config, $commonConfig);
