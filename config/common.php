<?php

$commonConfig =
[
    'aliases' => [
         '@usersdata' => '@app/users-data',
         '@audiocontents' => '@usersdata/audio-contents'
    ],

    'components' =>
    [
        'db' => require(__DIR__ . '/db.php'),
        'os' => 'app\components\OSHelper',
        'dateCheck' => 'app\components\DateChecker',
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user', 'teacher'],
        ],
    ]

];


return $commonConfig;