<?php

namespace app\rbac;

use yii\rbac\Rule;

/**
 * Checks if authorID matches user passed via params
 */
class AuthorRule extends Rule
{
    public $name = 'isAuthor';

    public function execute($user, $item, $params)
    {   
        $canDo = isset($params['comment']) ? $params['comment']->author == $user : false;
        return $canDo;
    }
}