<?php
namespace app\rbac;

use Yii;
use yii\rbac\Rule;

class UserRoleRule extends Rule
{
    public $name = 'userRole';

    public function execute($user, $item, $params)
    {   
        if(!Yii::$app->user->isGuest){
            //l'utente deve essere autenticato
            if($item->name == 'teacher'){
                //recuperiamo l'attributo role dell'utente corrente
                $role = Yii::$app->user->identity->role;
                return $role == 'teacher';
            }else if($item->name == 'user'){
                //tutti gli utenti hanno ruolo user
                return true;
            }
        }
        return false;
    }
}