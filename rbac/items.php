<?php
return [
    'deleteComment' => [
        'type' => 2,
        'description' => 'Delete comment',
        'children' => [
            'deleteOwnComment',
        ],
    ],
    'deleteOwnComment' => [
        'type' => 2,
        'description' => 'Delete own comment',
        'ruleName' => 'isAuthor',
    ],
    'user' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'deleteOwnComment',
        ],
    ],
    'teacher' => [
        'type' => 1,
        'ruleName' => 'userRole',
        'children' => [
            'deleteComment',
            'user',
        ],
    ],
];
