<?php

$this->title = "Synchronization $model->id";
$rows = (new \yii\db\Query())
    ->select(['id','title','type'])
    ->from(\app\models\UserContent::tableName())
    ->where(['id'=>$model->content_ids])
    ->all();

$referenceID = $model->content_ids[0];
$referenceContentRow = null;
foreach($rows as $row)
{
    if($row['id'] == $referenceID){
        $referenceContentRow = $row;
        break;
    }
}

function renderResultRow($row, $result, $isReference = false)
{
    $activeClass = $isReference?"active":"";
    $delay = $isReference?"":  $result->delay." seconds"; 
    ?>
    <tr>
        <th scope='row'> <?= $row['id'] ?> </th>
        <td> <?= $row['title'] ?> </td>
        <td> <?= $isReference?"reference":$result->delay ?> </td>
    </tr>
    <?php
}

?>

<div class='well col-lg-12'>
    <h3> Content synchronization </h3>

    <ul class='list-group'>
        <li class="list-group-item"><span class='font-weight-bold'>Synchronization id:</span>&nbsp; <?= $model->id; ?></li>
        <li class="list-group-item"><span class='font-weight-bold'>Created at:</span>&nbsp; <?= $model->created_at; ?></li>
        <li class="list-group-item"><span class='font-weight-bold'>Completed at:</span>&nbsp;  <?= $model->completed_at; ?></li>
    </ul>
    <br>
    <h4> Results: </h4>
    <table class="table table-bordered">
    <thead> 
        <tr> <th>#</th> <th>Content title</th> <th>Delay (s)</th> </tr>
    </thead>
    <?php
    renderResultRow($referenceContentRow, null, true);
    foreach($rows as $row){
        if($row['id'] == $referenceID){
            continue;
        }
        $result = $model->getResultFor($row["id"]);
        renderResultRow($row, $result);
    }
    ?>
    </table>
</div>

