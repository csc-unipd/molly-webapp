<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ClassModel */

$this->title = 'Create Class Model';
 $dossierLoadJs = "$.material.init();";
    $this->registerJs($dossierLoadJs);
?>
<br>
<div class="class-model-create well col-lg-12">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
