<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ClassModel */
$class = $model;
$this->title = $model->title;

\app\assets\ClassAsset::register($this);

?>     
   
    <div class='col-lg-4 '>
        <div class='well'>
            <h3> <i class="material-icons">school</i> <?= $class->title ?></h3>
            <hr>
            <?= $class->description ?>
            <hr>
            <span>created by</span><br>
            <span class='font-weight-bold'><?= $class->getCreator()->getFullName() ?></span>
            <span class='date-time text-muted font-italic created_time' data-time='<?= $class->created_at ?>'></span>
        </div>
    </div>
    
    <div id='dossier-contents-container' class='col-lg-8'>
        <div class='row no-gutters'>
            <div class='col-lg-12 '>  
                <div class='row  well no-gutters'>
                    <div class='col-lg-9 '>
                        <h4 class='align-middle'>Dossier list</h4>
                        <hr>
                    </div>
                    <div class='col-lg-3 text-right'>
                        <!--Add a content button-->
                        <a  href="<?= Url::to(['dossier/new', 'class_id' => $class->id]); ?>"                         
                            class="btn btn-fab btn-primary btn-raised add-content-btn ">
                            <i class="material-icons">add</i>
                        </a>
                    </div>
                </div>                                                
            </div>
            <?php
                $classModelJson = yii\helpers\Json::encode($class);
                $initClassViewerScript = "$.material.init(); var classViewer = new ClassViewer( $classModelJson );";
                $this->registerJs($initClassViewerScript);
            ?>
            
            <div id='dossiers-list' class='col-lg-12'>           
                <script id='dossier-list-template' type='x-tmpl-mustache'>
                {{#dossiers}}
                <div id='dossier_{{id}}' class='row well dossier-item no-gutters' style='padding-bottom: 4px;' data-dossier-id={{id}}>
                        <div class='col-lg-10'>           
                                <h5>
                                    {{#title}}
                                        {{title}}
                                    {{/title}}
                                    {{^title}}
                                        untitled dossier
                                    {{/title}}
                                </h5>
                                
                                <span>by</span>
                                <span class='font-weight-bold'>{{creator.full_name}}</span>
                                <span class='date-time text-muted font-italic' data-time='{{created_at}}'>{{created_at}}</span>
                                <hr>
                                <div class="btn-group" role="group" style='margin-bottom:0px'>
                                    {{#delete}}
                                        <a href='{{delete}}' class="btn btn-secondary btn-xs" style='padding:0px'>
                                            <i class="material-icons">delete</i>
                                        </a>
                                    {{/delete}}
                                </div>
                        </div>
                        <div class='col-lg-2 text-right'>
                            <a href='{{action_view}}' class='btn btn-default btn-sm'>
                                <i class="material-icons">navigate_next</i>
                            </a>
                        </div>
                    </div>
                 {{/dossiers}}
                </script>
            </div>                            
        </div>
        <div id="no-contents"></div>
</div>



