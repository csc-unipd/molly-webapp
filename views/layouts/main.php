<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    // NavBar::begin([
    //     'brandLabel' => 'Molly',
    //     'brandUrl' => Yii::$app->homeUrl,
    //     'options' => [
    //         'class' => 'navbar navbar-toggleable-sm bg-faded',
    //     ],
    // ]);
    // if(!Yii::$app->user->isGuest){
    //     $user = Yii::$app->user->identity;
    //     $fullName = $user->first_name." ".$user->last_name;
    // }

    // echo Nav::widget([
    //     'options' => ['class' => 'navbar-nav navbar-right'],
    //     'items' => [
    //         ['label' => 'Home', 'url' => ['/site/index']],
    //         ['label' => 'About', 'url' => ['/site/about']],
    //         ['label' => 'Contact', 'url' => ['/site/contact']],
    //         Yii::$app->user->isGuest ? (
    //             ['label' => 'Login', 'url' => ['/site/login']]
    //         ) : (
    //             '<li>'
    //             . Html::beginForm(['/site/logout'], 'post',['class'=>'form-inline'])
    //             . Html::submitButton(
    //                 'Logout (' . $fullName . ')',
    //                 ['class' => 'btn btn-link']
    //             )
    //             . Html::endForm()
    //             . '</li>'
    //         )
    //     ],
    // ]);
    // NavBar::end();
    ?>
    <nav class='navbar fixed-top navbar-toggleable-sm'>
        <a class="navbar-brand" href="/">Molly</a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <i class="material-icons">menu</i>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
        
            <ul class="navbar-nav mr-auto">
            </ul>
            
            <ul class="navbar-nav">
             <li class="nav-item">
                <a class='nav-link' href='/site/about'>About</a>
             </li>
                <li class="nav-item">
            <?php
            if(!Yii::$app->user->isGuest){
                $user = Yii::$app->user->identity;
                $fullName = ucfirst($user->first_name)." ".ucfirst($user->last_name);
            }
            echo  Yii::$app->user->isGuest ? 
                    "<a class='nav-link text-white' href='".Url::to(['site/login'])."'>Login</a>"
                : 
                    "<form class='form-inline ' method='post' action='".Url::to(['site/logout'])."'><button class='btn btn-link text-white' type='submit'>Logout( $fullName ) <i class='material-icons'>power_settings_new</i></button></form>";
                    ?>
            </li>    
            </ul>
        </div>
    </nav>
    <nav class='navbar '>
        <a class="navbar-brand" href="/">PlaceHolder</a>     
    </nav>
    <div class="container-fluid" > 
        <div class='row '>
            <div class='col-lg-12 hidden-md-down' style='height:20px'></div>
            <div class='col-lg-8 offset-lg-2'>
                <div class='row main-content'>
                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                    ]) ?>
                    <?= $content ?>
                </div>
            </div>
        </div>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="float-left">&copy; Molly <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
