<?php

use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index col-lg-12 text-center">

        
        <a class="btn btn-lg btn-info btn-raised btn-lg" href='<?= Url::to(['site/login']) ?>'>Sign in</a>
        <a class="btn btn-lg btn-danger btn-raised btn-lg" href='<?= Url::to(['user/new']) ?>'>Sign up</a>

</div>
