<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About MOLLY';
?>
<div class="site-about card col-lg-12">
<div class='card-block'>
    <h1><?= Html::encode($this->title) ?></h1>
    <br>
    <style>
        p{text-indent: '1.5em'}
    </style>
    <p>
       L’obiettivo del progetto MOLLY è la realizzazione di una piattaforma informatica per l’apprendimento
        digitale mirata a favorire la partecipazione attiva degli studenti e l’interazione con la
realtà studiata. </p>

<p>
Vuole essere uno strumento didattico che consente agli alunni di muoversi sul territorio per
andare a mappare dei luoghi con fotograﬁe, video, registrazioni audio e documenti testuali.
</p>
<p>
Gli insegnanti avrebbero la facoltà di aprire un nuovo corso (o classe) al quale aggiungerebbero
 gli studenti partecipanti e altri insegnanti collaboratori.
 </p>
 <p>
All’interno del corso potrebbero aggiungere un nuovo tema di mappatura che descriverebbe il luogo e le caratteristiche da stu-
diare. In questo tema ogni studente, oltre agli insegnanti, potrebbe inserire i contenuti da lui
creati afﬁnché siano visibili, commentabili e valutabili dagli altri. L’insieme di questi contenuti
multimediali formerebbe la base su cui gli studenti, insieme agli insegnanti, realizzerebbero un
documento (articolo o report) del luogo studiato che chiameremo dossier.
</p>
<p>
Insegnanti e studenti potrebbero interagire fra loro commentando e recensendo i contenuti
altrui con lo scopo di migliorare la qualità complessiva del dossier. Terminata l’attività un sot-
toinsieme dei contenuti formerebbe il dossier, che potrà essere reso pubblico o meno. Un dossier
pubblicato sarà visibile a chiunque via web.
</p>
<p>
I genitori degli studenti potranno accedere al sistema e visionare il proﬁlo dei loro ﬁgli
osservando i contenuti da loro creati e le valutazioni che hanno ricevuto.
Studenti, genitori e professori potranno comunicare tra loro attraverso un sistema di messag-
gistica.
</p>
<p>
Il target degli studenti a cui è rivolto il progetto è composto da bambini e ragazzi con età
compresa fra i 6 e i 18 anni.
    </p>

</div>
</div>
