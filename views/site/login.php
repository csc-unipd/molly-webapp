<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
// $this->params['breadcrumbs'][] = $this->title;

 $dossierLoadJs = "$.material.init();";
    $this->registerJs($dossierLoadJs);
?>
<div class="col-lg-8 offset-lg-2">
<div class="site-login well">
    <h4><i class="material-icons">lock</i> <?= Html::encode($this->title) ?></h4>

    <!--<p>Please fill out the following fields to login:</p>-->

    <?php $form = ActiveForm::begin([
        'enableClientValidation' => true,
        'enableAjaxValidation' => false,
        'validateOnSubmit' => false,
        'options' => [
           // 'enctype' => 'multipart/form-data'
        ],
        'fieldConfig' => [
            'options'=>[
              //  'class' =>'input-group'
            ],
            'errorOptions' => ['class' => 'form-control-feedback'],
            'template' => "{label}\n{input}{hint}\n{error}"   
        ],

        'errorCssClass'=> 'has-warning'
    ]
        ); ?>


        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

        <?= $form->field($model, 'password')->passwordInput() ?>

        <?= $form->field($model, 'rememberMe')->checkbox([
            'template' => "<div class=\"col-lg-offset-1 col-lg-3\">{input} {label}</div>\n<div class=\"col-lg-8\">{error}</div>",
        ]) ?>

        <div class="form-group float-right">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-raised btn-lg', 'name' => 'login-button']) ?>
        </div>
        <div class="clearfix"></div>


    <?php ActiveForm::end(); ?>

</div>
</div>
