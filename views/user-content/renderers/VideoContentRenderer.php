<?php 
namespace renderers;

require_once 'DefaultRenderer.php';

use yii;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

class VideoContentRenderer extends DefaultRenderer {
	
	public function render($model){
		?>
		<div style='text-align:center; background: black; margin-bottom:2em;'>
	        <video width='1024' height='720' controls style='margin:auto; display:block'>
	            <source src="<?=  Url::to(['user-content/file', 'type'=>'video', 'id' => $model->id]);?>" type='video/mp4'>
	        </video>
	    </div>
		<?php
		$this->renderDetailView($model);
	}
	
}

?>