<?php 
namespace renderers;

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii;

class DefaultRenderer {
	
	public function render($model){
		$this->renderDetailView($model);
	}
	
	public function renderDetailView($model)
	{
		?>
				<p>
				<?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
		        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
		            'class' => 'btn btn-danger',
		            'data' => [
		                'confirm' => 'Are you sure you want to delete this item?',
		                'method' => 'post',
		            ],
		        ]) ?>
				</p>
				
				<?= Yii::$app->controller->renderPartial('awarenessContext', ['awarenessContext' => $model->awareness_context]);?>
			    <?= DetailView::widget([
			        'model' => $model,
			        'attributes' => [
			            'id',
			            'title',
			            'file_path',
			            'created_at',
			            'updated_at',
			        ],
			    ]) ?>
				    
				<?php
				
	}
	
}

?>