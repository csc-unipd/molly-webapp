<?php 
namespace renderers;

require_once 'DefaultRenderer.php';

use yii;
use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;

class AudioContentRenderer extends DefaultRenderer {
	
	public function render($model){
		echo Yii::$app->controller->renderPartial('waveform', ['model' => $model]);
		$this->renderDetailView($model);
	}
	
}

?>