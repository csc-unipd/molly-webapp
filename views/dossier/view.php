<?php

  use yii\helpers\Url;
  use yii\helpers\Json;
    $jsFile = "@web/js/jquery.ui.widget.js";
    $this->registerJsFile($jsFile, ['depends' => [\yii\web\JqueryAsset::className()]]);

    $jsFile = '@web/js/peaks.js';
    $this->registerJsFile($jsFile, [
        'depends' => [\app\assets\AppAsset::className()]
        ]);
    $dossierLoadJs = "$.material.init(); var viewer = new DossierViewer( ".Json::encode($dossier)." );";
    $this->registerJs($dossierLoadJs);

    \app\assets\DossierAsset::register($this);

    $contentItemTemplate = file_get_contents(__DIR__.'/content_item_template.mst');
    $awarenessContextTemplate = file_get_contents(__DIR__.'/awareness_context_template.mst');

    $this->title = $dossier->title;
?>
    <div class='col-lg-4 '>
        <div class='row no-gutters'>
        <div class='col-lg-12 '>
                <a href="<?= Url::to(['class/view', 'id'=>$dossier->class]); ?>" class='btn bg-inverse text-white btn-block btn-raised text-left'>
                    <i class="material-icons">chevron_left</i>
                    <i class="material-icons">school</i>
                    <span class='badge hidden selected-counter'>View class</span>
                </a>
            </div>
        </div>

        <div class='well'>
            <h3><?= $dossier->title ?></h3>
            <hr>
            <?php if(!empty($dossier->description)){ ?>
                <?= $dossier->description ?>
                <hr>
            <?php } ?> 
            <span>created by</span><br>
            <span class='font-weight-bold'><?= $dossier->getCreator()->getFullName() ?></span>
            <span class='date-time text-muted font-italic created_time' data-time='<?= $dossier->created_at ?>'></span>
        </div>

        <div class='row no-gutters'>
            <div class='col-lg-12 '>
                        <button  class="btn btn-info btn-block synchronize-btn btn-raised disabled hidden-md-down">
                            <i class="material-icons align">sort</i>
                            <i class="material-icons loop">loop</i>
                            <span class='badge hidden selected-counter'>0 Contents Selected</span>
                        </button>
            </div>
        </div>
    </div> 
    <div id='dossier-contents-container' class = 'col-lg-8'>
        <div class='row no-gutters'>
            <div class='col-lg-12'>          
                                <div class='row well no-gutters'>
                                    <div class='col-lg-9'>
                                            <h4> Contents list </h4>
                                            <hr>
                                        </div>                
                                <div class='col-lg-3 text-right'>
                                    <!--Add a content button-->
                                    <a  href="/dossier/new-content?id=<?= $dossier->id ?>"
                                        
                                        class="btn btn-fab btn-primary btn-raised add-content-btn">
                                        <i class="material-icons">create</i>
                                    </a>
                        </div>
            </div>
            <div class='row no-gutters'>
                <div class='contents-list col-lg-12'>
                    <div class="loader centered"></div>           
                </div>
                <button class='btn btn-block btn-info show-more' style='display: none'><i class="material-icons">expand_more</i>Show more</button>
            </div>
            <div id="no-contents" ></div>
            <!--Write the content item template-->
            <script id='content-item-template' type='x-tmpl-mustache'>
                <?= $contentItemTemplate; ?>
            </script>
            <!--Write the awareness context template-->
            <script id='awareness-context-template' type='x-tmpl-mustache'>
                <?= $awarenessContextTemplate ?>
            </script>
            <script id='comment-template' type='x-tmpl-mustache'>
               <?= file_get_contents(__DIR__.'/content_comment_template.mst');?>
            </script>

            <div id='message-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class='modal-header'>
                                <h4>Message</h4>
                        </div>
                        <div class='modal-body'>
                            <span class='message'></span>
                        </div>
                        <div class='modal-footer'>
                            <button type='button' class='btn btn-primary' data-dismiss='modal'>Ok</button>
                        </div>
                    </div>
                </div>
            </div>

            <div id='delete-modal' class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class='modal-header'>
                                <h4>Delete content</h4>
                        </div>
                        <div class='modal-body'>
                            <small>Do you want to delete the content:</small>
                            <br>
                            <strong>{{title}}<strong>
                        </div>
                        <div class='modal-footer'>
                            <button type='button' class='btn btn-default' data-dismiss='modal'>Cancel</button>
                            <a href='{{delete}}' class='btn btn-primary'>Confirm</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>


