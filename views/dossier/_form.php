<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\AudioContent */
/* @var $form yii\widgets\ActiveForm */

$jsFile = "@web/js/jquery.ui.widget.js";
$this->registerJsFile($jsFile, ['depends' => [\yii\web\JqueryAsset::className()]]);
$jsFile = '@web/js/AwarenessSnapshot.js';
$this->registerJsFile($jsFile, ['depends' => [\yii\web\JqueryAsset::className()]]);
$jsFile = '@web/js/filecontent-form.js';
$this->registerJsFile($jsFile, ['depends' => [\yii\widgets\ActiveFormAsset::className()]]);

 $dossierLoadJs = "$.material.init();";
 $this->registerJs($dossierLoadJs);
?>
  
    <?php $form = ActiveForm::begin([
        'enableClientValidation' => true,
        'enableAjaxValidation' => true,
        'validateOnSubmit' => false,
        'options' => [
            'enctype' => 'multipart/form-data'
        ],
        'fieldConfig' => [
            'options'=>[
              //  'class' =>'input-group'
            ],
            'errorOptions' => ['class' => 'form-control-feedback'],
            'template' => "{label}\n{input}{hint}\n{error}"   
        ],

        'errorCssClass'=> 'has-warning'
    ]
        ); ?>
    
    <div class="form-group">
        <?= $form->field($model, "title")->input("text",['autocomplete'=>'off']); ?>
    </div>
     <div class="form-group">
        <?= $form->field($model, "description", ['enableAjaxValidation' => false])
            ->textArea(['rows' => 7,'style'=>'resize:none']);
             ?>
    </div>
    <div class='form-group'> 
     <?php
        echo $form->field($model, 'file', ['template'=>"
            
            <button id='file-chooser-btn' class='btn btn-file btn-primary btn-raised'>
                <i class='material-icons'>attach_file</i> Add file content            
            </button>
            {input}
            <span class='selected-file text-success'></span>\n
            {error}
            "])->fileInput(['class'=>'form-control-file']);
    ?>
    </div>
    <div class="form-group">
        <?= Html::submitInput(
            $model->isNewRecord ? 'Create' : 'Update',
            ['class' => 'btn btn-block btn-lg btn-primary btn-raised',
                'name'=>'submit'
            ]
        ) ?>
    </div>

    <div class="form-group" style='margin-top:5px'>    
        <label for="AwarenessContext" class="control-label">Awareness Context</label><br>
        <span class='awareness-snapshot-progress-info' style='color:red'>No awareness data</span><br>
        <input type="hidden" class="form-control" name="AwarenessContext">
        <pre class='snapshot-data'></pre>
    </div>

    <?php
        ActiveForm::end();
        $id = $form->options['id'];
       // $this->registerJs("jQuery('#$id').myActiveForm();");
    ?>


