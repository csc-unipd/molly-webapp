<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'New dossier';

$dossierLoadJs = "$.material.init();";
$this->registerJs($dossierLoadJs);

?>
<div class="content-create col-lg-8 offset-lg-2">
<br>
<div class="well">
    <h4><?= Html::encode($this->title) ?></h4>

    <!--<p>Please fill out the following fields to login:</p>-->

    <?php $form = ActiveForm::begin([
            'enableClientValidation' => true,
            'enableAjaxValidation' => false,
            'validateOnSubmit' => false,
            'options' => [
            // 'enctype' => 'multipart/form-data'
            ],
            'fieldConfig' => [
                'options'=>[
                //  'class' =>'input-group'
                ],
                'errorOptions' => ['class' => 'form-control-feedback'],
                'template' => "{label}\n{input}{hint}\n{error}"   
            ],

            'errorCssClass'=> 'has-warning'
        ]
        ); ?>
        <div class="form-group">
            <?= $form->field($dossier, 'title')->textInput(['autofocus' => true]) ?>
        </div>
        <div class="form-group">
            <?= $form->field($dossier, 'description')->textArea() ?>
        </div>
        <div class="form-group float-right">
                <?= Html::submitButton('Create', ['class' => 'btn btn-primary btn-raised btn-lg']) ?>
        </div>
        <div class="clearfix"></div>
    <?php ActiveForm::end(); ?>
</div>
</div>
