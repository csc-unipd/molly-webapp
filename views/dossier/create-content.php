<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $type string*/
/* @var $model app\models\AudioContent */

$this->title = "Create Content";
// $this->params['breadcrumbs'][] = ['label' => "Contents", 'url' => ['index']];
// $this->params['breadcrumbs'][] = $this->title;
?>
<div class="content-create col-lg-8 offset-lg-2">
    <br>
    <div class="well">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
    </div>
</div>
