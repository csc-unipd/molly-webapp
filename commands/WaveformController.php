<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\UserContent;

class WaveformController extends Controller
{

    public function actionGenerate($audiocontentID)
    {
        $model = UserContent::findOne(['id' => $audiocontentID, 'type' => 'audio']);
        if($model){
            
            $filePath = Yii::getAlias("@app").$model->getResourceUri('source');
            $audioInfo = $this->audioInfo($filePath);
            Yii::info(print_r($audioInfo, true));
            print_r($audioInfo);
            $originalSampleRate = $audioInfo["sample_rate"]?$audioInfo["sample_rate"] : 8000;
            $pixelsPerSecond = 2000;
            $samplesPerGroup = floor( $originalSampleRate / $pixelsPerSecond);

            $command = "ffmpeg -i \"$filePath\" -loglevel quiet -map 0:a:0 -ac 1 -c:a pcm_s8 -f data -";
            


            Yii::info('exec: '.$command);
            $commandOutputStream = popen($command, 'rb');
            $waveformFile = Yii::getAlias("@app").$model->getResourceUri('waveform');

            $waveformFileHandle = fopen($waveformFile, 'wb');

            fwrite($waveformFileHandle, "{\"sample_rate\": $originalSampleRate,");
            fwrite($waveformFileHandle, "\"samples_per_pixel\": $samplesPerGroup,");
            fwrite($waveformFileHandle, "\"bits\": 8,");
            fwrite($waveformFileHandle, "\"data\": [");

            $nPixels = 0;

            
            while(!feof($commandOutputStream))
            {
                $groupSamplesString = fread($commandOutputStream, $samplesPerGroup);
                
                if($groupSamplesString)
                {
                    if($nPixels!=0){
                            fwrite($waveformFileHandle, ",");
                    }
                    //we need to use the unpack command
                    //otherwise we cannot retrieve signed bytes from the readed string
                    //'c*'' format stand for a signed byte ('c' symbol) repeated unlimited times ('*' symbol)

                    $groupSamples = unpack('c*', $groupSamplesString);
                    $min = $max = $groupSamples[1];
                    $lastIndex = max(array_keys($groupSamples));

                    for($i = 2; $i <= $lastIndex; ++$i)
                    {
                        $min = min($min, $groupSamples[$i]);
                        $max = max($max, $groupSamples[$i]);
                    }
                    fwrite($waveformFileHandle, join(',', [$min,$max]));
                    $nPixels++;
                }else{
                    if(feof($commandOutputStream)){
                        echo "feof and exact read occured";
                    }
                }
            }
            
            fwrite($waveformFileHandle, "],");
            fwrite($waveformFileHandle, "\"length\": $nPixels");
            fwrite($waveformFileHandle, "}");

            fclose($waveformFileHandle);
            fclose($commandOutputStream);
            
            chmod($waveformFile, 0775);
            
            }else{
                echo 'Please specify a valid content id';
                return 1;
            }
    }

      public function audioInfo($filePath)
    {
        $audioFile = $filePath;
        $command = "ffprobe -i \"$filePath\" -show_streams -select_streams a:0 -v error";

        $responseText = shell_exec($command);
        $lines = preg_split("(\n|\r\n)", $responseText);

        $infos = [];

        foreach($lines as $line)
        {
                $keyValuePair = explode('=', $line, 2);

                if(!empty($keyValuePair) && count($keyValuePair)==2){
                    $key = $keyValuePair[0];
                    $value = $keyValuePair[1];
                    $infos[$key] = $value;
                }
                // $matches = [];
                // $letter = "A-Za-z";
                // $number = "0-9";
                // $tagPattern = "[$letter\=\-]+[$letter$number\s\=\-]*[$letter$number\=\-]";
                // $valuePattern = "[$letter$number\\\=\-'\"\.\:]+[$letter$number\\\=\-'\"\.\:\s]*[$letter$number\\\=\-'\"\.\:]";
                // $pattern = "/^\s*($tagPattern)\s*?\:\s*($valuePattern).*$/";

                // if(preg_match($pattern,$line, $matches))
                // {
                //     if(count($matches) ==3){
                //         $tag = strtolower(preg_replace("/\s+/", '_', $matches[1]));
                //         $value = $matches[2];
                //         $infos[$tag] = $value;
                //     }
                    
                // }
        }

        return $infos;
    }

    public static function launchGeneration($id){
            chdir(Yii::getAlias('@app'));
            $yiiCommandlineScriptName = 'php ./yii';
            $cmd =  "$yiiCommandlineScriptName waveform/generate $id";
            // die($cmd);
            if($isWindows = Yii::$app->os->isWindows()){
                $launchCmd = "start /b cmd /c $cmd";
                Yii::info("Exec: ". $launchCmd);
                pclose(popen($launchCmd, 'rb'));
            }else{
                $launchCmd = "nohup $cmd &";
                Yii::info("Exec: ". $launchCmd);
                exec($launchCmd);
            }

    }
}