<?php
namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        
        //$authorRule definisce se l'utente è l'autore di un commento
        $authorRule = new \app\rbac\AuthorRule;
        $auth->add($authorRule);

        //permesso di cancellare un commento
        $deleteComment = $auth->createPermission('deleteComment');
        $deleteComment->description = 'Delete comment';
        $auth->add($deleteComment);

        //permesso di cancellare un commento se l'utente è l'autore
        $deleteOwnComment = $auth->createPermission('deleteOwnComment');
        $deleteOwnComment->description = 'Delete own comment';
        $deleteOwnComment->ruleName = $authorRule->name;
        $auth->add($deleteOwnComment);

        //$deleteComment usa anche il permesso $deleteOwnComment
        $auth->addChild($deleteOwnComment, $deleteComment);
       
        //definisco i ruoli all'interno dell'applicazione
        //$rule è la regola che definisce se l'utente
        //appartiene a un certo gruppo
        $rule = new \app\rbac\UserRoleRule;
        $auth->add($rule);

        $user = $auth->createRole('user');
        $user->ruleName = $rule->name;
        $auth->add($user);

        $teacher = $auth->createRole('teacher');
        $teacher->ruleName = $rule->name;
        $auth->add($teacher);

        //lego tutto associando permessi e ruoli
        $auth->addChild($user, $deleteOwnComment);
        $auth->addChild($teacher, $deleteComment);

         //teacher ha anceh i permessi di user
        $auth->addChild($teacher, $user);
    }
}