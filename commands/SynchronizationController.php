<?php

namespace app\commands;

use Yii;
use yii\console\Controller;
use app\models\AudioContentSynchronization;
use app\models\AudioContent;

class SynchronizationController extends Controller
{

    public function actionExecute($audiocontentSynchronizationID)
    {
        $model = AudioContentSynchronization::findOne($audiocontentSynchronizationID);
        $audioFiles = [];
        foreach($model->content_ids as $id)
        {
            $audioContent = AudioContent::findOne($id);
            $filePath = $audioContent->buildPath('audiofile');
            echo "$filePath\n";
            $audioFiles[] = "\"$filePath\"";
        }
        chdir(Yii::getAlias("@app"));

        $command = 'java -jar .\bin\audioalign.jar';
        $command = join(' ', array_merge([$command],$audioFiles));

        echo "ready to execute command: \"$command\"\n";

        $pstream = popen($command, "rb");
        while(($char = fgetc($pstream)) !== FALSE)
        {
            echo $char;
        }
        pclose($pstream);
    }
}