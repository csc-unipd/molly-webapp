<?php

namespace app\controllers;

use yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\helpers\Url;
use app\models\FileContent;
use app\models\AudioContent;
use app\models\UserContent;
use app\models\AwarenessContext;
use yii\web\Response;
use yii\widgets\ActiveForm;


class UserContentController extends Controller
{
	
    
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                   // 'delete' => ['POST'],
                ],
            ],
        ];
    }   

    public function actionResource($id, $resource)
    {
       
        $content = UserContent::findOne($id);
       
        if(!$content){     
            throw new \yii\web\NotFoundHttpException();
        }
        
        $resourceUri = $content->getResourceUri($resource);

       
        if(!$resourceUri){
            throw new \yii\web\BadRequestHttpException();
        }
        $response = Yii::$app->getResponse();
        $response->format = \yii\web\Response::FORMAT_RAW;

        $mimeType = FileHelper::getMimeTypeByExtension($resourceUri);
 
        $response->getHeaders()->set('Content-Type', $mimeType);
        $response->getHeaders()->set('X-Accel-Redirect', $resourceUri);
        
        return $response->send();
    }

     public function actionAwarenessContext($id){
        $response = Yii::$app->response;
        $response->format = Response::FORMAT_JSON;

        $userContent = UserContent::findOne($id);
        if($userContent && $userContent->awareness_context){

            $awContext = AwarenessContext::findOne($userContent->awareness_context);
            return $awContext;
        }
              
        throw new \yii\web\NotFoundHttpException();
    }
    
     /**
     * Deletes an existing AudioContent model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = UserContent::findOne($id);
    
        if(!$model){
            throw new yii\web\NotFoundHttpException;
        }
        $dossierID = 
        $row = (new \yii\db\Query())
            ->select(['dossier'])
            ->from('dossier_content_map')
            ->where(['content' => $model->id])
            ->one();
        
        if($model->delete()){
            if($row){
                return $this->redirect(Url::to(['dossier/view', 'id'=> $row['dossier']]));
            }
           return $this->redirect(Yii::$app->request->referrer);
        };
    }
}
