<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\UploadedFile;
use yii\helpers\FileHelper;
use yii\web\Response;
use yii\widgets\ActiveForm;

use app\models\UserContent;
use app\models\User;
use app\models\AwarenessContext;
use app\models\Dossier;

class DossierController extends Controller
{

    public function behaviors(){

        return[
            'access' =>[
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //user must be authenticated in order to use this actions
                        'allow' => true,
                        'roles' => ['@']  
                    ]   
                ]
            ]
        ];
    }

    public function actionNew($class_id){

        $dossier = new Dossier();
        
        $request = Yii::$app->request;

        if($request->isPost){
            //form is filled
            $dossier->load($request->post());
            $dossier->class = $class_id;
             $dossier->creator = Yii::$app->user->getID();
            if($dossier->save()){
                $this->redirect(['dossier/view', 'id'=>$dossier->id]);
            }
        }

        return $this->render('new', ['dossier'=> $dossier]);
    }

     public function actionDelete($id){

        $dossier = Dossier::findOne($id);
        if($dossier){
            $dossier->delete();
            $classID = $dossier->class;

            return $this->redirect(['class/view', 'id'=>$classID]);
        }
        
        throw new \yii\web\NotFoundHttpException();
    }

    public function actionView($id){
        $dossier = Dossier::findOne($id);

        if(!$dossier)
            throw new \yii\web\NotFoundHttpException("Dossier $id not found");

        return $this->render('view', ['dossier'=>$dossier]);
    }

    public function actionNewContent($id, $type= 'audio'){
        //time bookmark
        $a = $b = $c = $d= $e = 0;
        $a = microtime(true);
        ini_set('max_execution_time', '999');
    	ini_set('memory_limit', '128M');

        $content = new UserContent(['scenario' => UserContent::$SCENARIO_CREATE_DOSSIER]);
        $dossier = new Dossier();
        $dossier->id = $id;

        $request = Yii::$app->request;
        if($request ->isPost){

            $content->load($request ->post());
            $content->file = UploadedFile::getInstance($content, 'file');
            $b = microtime(true);
            if($awarenessContextData = Yii::$app->request->post('AwarenessContext'))
    		{
    			$content->awareness_context = new AwarenessContext();
    			$content->awareness_context->load(json_decode($awarenessContextData, true), '');
    		}

            $isValidationRequest = $request->post("ajax") !== null;
    		
    			Yii::$app->response->format = Response::FORMAT_JSON;
    			$messages = ActiveForm::validate($content);
                if(!empty($messages) || $isValidationRequest){
                    return $messages;
                }
            

                $content->creator = Yii::$app->user->getId();
                $c = microtime(true);

                if($content->file != null){
                    $content->type = UserContent::getFileType($content->file->tempName);
                    $internalDir = $dossier->getContentsInternalDirectory();

                    $content->mime_type = FileHelper::getMimeType($content->file->tempName, null, false);
                    $mimeExtensions = FileHelper::getExtensionsByMimeType($content->mime_type);

                    $ext = $content->file->extension;
                    if(!$ext || empty($ext)){
                        $content->file->extension = UserContent::getExtensionFor($content->file->tempName);
                    }

                    $absolutePath;
                    do{
                        $fileName = UserContent::generateRandomFilename($ext);
                        $content->file_path = $internalDir.'/'.$content->type.'/'.$fileName;

                        $absolutePath = Yii::getAlias('@app')."$content->file_path";
                        $exist = file_exists($absolutePath);
                    }while ($exist);
                    $d = microtime(true);
                    if(!$content->file->saveAs($absolutePath)){
                         throw new \yii\web\ServerErrorHttpException("Cannot save content file");
                    }
                    $e = microtime(true);

                    if(!chmod($absolutePath, 0775)){
                        die("chmod failed");
                    };

                }else{
                    $content->type = 'text';
                    $content->mime_type = 'text/plain';
                }
                
                //success
                //save the record now
                if($content->save(FALSE)){
                    
                    //save dossier x content relation
                    $command = Yii::$app->db->createCommand();

                    $command->insert('dossier_content_map',[
                        'dossier'=> $dossier->id,
                        'content'=> $content->id,
                    ])->execute();

                    $e = microtime(true);
                    echo 'e-a: '.($e-$a)."\n";
                    echo 'e-b: '.($e-$b)."\n";
                    echo 'e-c: '.($e-$c)."\n";
                    echo 'e-d: '.($e-$d)."\n";
                    
                    return $this->redirect(['dossier/view', 'id' => $dossier->id]);
                }
            }            


        return $this->render('create-content', [
                'dossier' => $dossier,
    			'model' => $content,
    			'type' => $type
    	]);
    }

    /*
    * get multimedia contents of this dossier in json format
    * given requested media type
    */
    public function actionContents($id,$before_id = null)
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;
    
        $dossier = Dossier::findOne($id);
        if(!$dossier){
            throw new \yii\web\NotFoundHttpException("Dossier not found");
        }   
        return $dossier->getContents($before_id);
    }

    public function actionItemTemplate($type)
    {
        return $this->renderPartial('item-template.mst.php', ['type'=>$type]);
    }

     /**
    * @return \yii\db\ActiveQuery
    */
   public function getClass0()
   {
       return $this->hasOne(ClassModel::className(), ['id' => 'class']);
   }
   /**
    * @return \yii\db\ActiveQuery
    */
   public function getDossierContentMaps()
   {
       return $this->hasMany(DossierContentMap::className(), ['dossier' => 'id']);
   }
   /**
    * @return \yii\db\ActiveQuery
    */
   public function getContents()
   {
       return $this->hasMany(UserContent::className(), ['id' => 'content'])->viaTable('dossier_content_map', ['dossier' => 'id']);
   }

}