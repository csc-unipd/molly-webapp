<?php

namespace app\controllers;

use yii;
use yii\web\Controller;
use app\models\ContentSynchronization;

class ContentSynchronizationController extends Controller
{
    
    public function actionNew()
    {
        $request = Yii::$app->request;
        if($request->isPost)
        {
            $model = new ContentSynchronization();    
            $model->load($request->post());
            if($model->save())
            {
                //launch synchronization process
                $model->run();
                
                //redirect to monitor page
                return $this->redirect(['monitor', 'id'=>$model->id]);
            }
        }
        throw new \yii\web\BadRequestHttpException();
    }


    public function actionMonitor($id)
    {
        $model = ContentSynchronization::findOne($id);
        if($model){
            return $this->render('monitor', ['model'=>$model]);
        }

        throw new \yii\web\NotFoundHttpException();
    }
}