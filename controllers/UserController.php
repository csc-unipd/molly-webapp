<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
         return[
            'access' =>[
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        //user must be authenticated in order to use this actions
                        'allow' => true,
                        'actions' =>['new'],
                        'roles' => ['?']  
                    ]   
                ]
            ]
        ];
    }


    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionNew()
    {
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {

            if($model->password != $model->passwordConfirmation){
                $model->addError('password', 'Passwords don\'t match');

            }else{
                $model->password_hash =  Yii::$app->getSecurity()->generatePasswordHash($model->password);

                if($model->save(false)){
                    
                    //per adesso registriamo tutti i nuovi utenti come iscritti
                    //alla classe di esempio che ha id=1
                    $classID = 1;
                    //save class x user relation
                    $command = Yii::$app->db->createCommand();

                    $command->insert('class_user_map',[
                        'class'=>  $classID ,
                        'user'=> $model->id,
                    ])->execute();

                    return $this->redirect(['class/view', 'id' => $classID]);
                }
                 //return $this->redirect(['view', 'id' => $model->id]);
            }
           

        } 

        return $this->render('new', [
            'model' => $model,
        ]);     
    }


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
