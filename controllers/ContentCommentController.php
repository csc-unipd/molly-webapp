<?php

namespace app\controllers;

use yii;
use app\models\ContentComment;
use yii\filters\AccessControl;

class ContentCommentController extends \yii\web\Controller{


    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'matchCallback' => function($rule, $action){
                            $id = Yii::$app->request->get('id');
                            if(!empty($id)){
                                $comment = ContentComment::findOne($id);
                                return $comment && Yii::$app->user->can('deleteComment', ['comment'=>$comment]);
                            }
                            return false;
                        },
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'new'],
                        'roles' => ['@']                     
                    ]
                ],
            ],
        ];
    }

    public function actionIndex($content_id, $before_date = null){
            $maxCommentsPerRequest = 20;

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            if(empty($before_date) || !Yii::$app->dateCheck->isDate($before_date)){
                $before_date = '9999-12-31';
            }

            $comments = ContentComment::find()
            ->andWhere('content_id=:cid',[':cid'=>$content_id])
            ->andWhere('created_at < :date',[':date'=>$before_date])
            ->orderBy('created_at DESC')
            ->limit($maxCommentsPerRequest)
            ->all();

            if(count($comments) < $maxCommentsPerRequest){
                $comments[] = null;
            }

            return $comments;
    }


    //azione per creare un nuovo commento
    //i dati vengono inviati via metodo POST
    public function actionNew()
    {
        $response = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;
        
        $newComment = new ContentComment();
        $newComment->load($request->post(),'');
        $newComment->author = Yii::$app->user->getId();
        $newComment->created_at = $newComment->updated_at = date("Y-m-d H:i:s", time());
        if( $newComment->save()){
            return $newComment;
        }else{

            //il commento non è valido, imposto lo status code
            //422 Unprocessable Entity e ritorno gli errori riscontrati
            $response->statusCode = 422;
            return $newComment->getErrors();
        }
    }

    public function actionDelete($id){
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $comment = ContentComment::findOne($id);

        if(!$comment){
            throw new \yii\web\NotFoundHttpException('Comment doesn\'t exist');
        }else if($comment->delete()){
            return 'Ok';
        }else if($comment){
             throw new \yii\web\ServerErrorHttpException();
        }
    }
}